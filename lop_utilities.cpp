#include "lop_utilities.h"

#include <OP/OP_Layout.h>
#include <OP/OP_Network.h>
#include <OP/OP_SubnetIndirectInput.h>

/*
	Run automatic layout on all nodes in the network.
*/
void lop_utilities::LayoutNodes(OP_Network *net)
{
	OP_Layout layout(net);
	/* Get indirect inputs. */
	int ni = net->getNparentInputs();
	for( int i = 0; i < ni; ++i )
	{
		layout.addLayoutItem(net->getParentInput(i));
	}
	/* Get nodes. */
	int nc = net->getNchildren();
	for( int i = 0; i < nc; ++i )
	{
		layout.addLayoutItem(net->getChild(i));
	}
	layout.layoutOps(OP_LAYOUT_TOP_TO_BOT, nullptr);
}

/*
	Connect node's first input to input_node's first output.
	If input_node is nullptr, connect to parent's first indirect input instead.
*/
void lop_utilities::ConnectInput(OP_Node *node, OP_Node *input_node)
{
	if( input_node )
	{
		node->setInput(0, input_node);
	}
	else
	{
		OP_SubnetIndirectInput *parent_input =
			node->getParentNetwork()->getParentInput(0);
		if( parent_input )
		{
			node->setIndirectInput(0, parent_input);
		}
	}
}
