node = kwargs.get("node", None)
template_group = node.parmTemplateGroup()
res_mode_parameter = hou.StringParmTemplate("res_mode", "Resolution Mode", 1)
if res_mode_parameter:
    #Set Parameters
    res_mode_parameter.setItemGeneratorScript("menu = __import__('loputils').resolutionModeMenuItems()\nreturn menu")
    res_mode_parameter.setScriptCallback("__import__('loputils').updateResolutionParameters(hou.pwd())")
    res_mode_parameter.setScriptCallbackLanguage(hou.scriptLanguage.Python)
    
    template_group.insertAfter("camera",res_mode_parameter)
    #Set the modified parameter template group back on the node
    
quality_folder = hou.FolderParmTemplate("folder", "Quality")
quality_folder.addParmTemplate(hou.IntParmTemplate("shading_samples", "Shading Samples", 1, default_value = (64,), min = 1, max = 400, min_is_strict = True))
quality_folder.addParmTemplate(hou.IntParmTemplate("pixel_samples", "Pixel Samples", 1, default_value = (8,), min = 1, max = 100, min_is_strict = True))
quality_folder.addParmTemplate(hou.IntParmTemplate("volume_samples", "Volume Samples", 1, default_value = (16,), min = 1, max = 400, min_is_strict = True))
quality_folder.addParmTemplate(hou.IntParmTemplate("max_diffuse_depth", "Max Diffuse Depth", 1, default_value = (2,), min = 0, max = 10, min_is_strict = True))
quality_folder.addParmTemplate(hou.IntParmTemplate("max_reflection_depth", "Max Reflection Depth", 1, default_value = (2,), min = 0, max = 10, min_is_strict = True))
quality_folder.addParmTemplate(hou.IntParmTemplate("max_refraction_depth", "Max Refraction Depth", 1, default_value = (4,), min = 0, max = 10, min_is_strict = True))
quality_folder.addParmTemplate(hou.IntParmTemplate("max_hair_depth", "max Hair Depth", 1, default_value = (5,), min = 0, max = 10, min_is_strict = True))
quality_folder.addParmTemplate(hou.IntParmTemplate("max_distance", "Max Distance", 1, default_value = (1000,), min = 0, max = 400, min_is_strict = True))


# Add the desired parameters as checkboxes

template_group.insertBefore("top_level_tabs", quality_folder)
node.matchCurrentDefinition()
node.setParmTemplateGroup(template_group)
