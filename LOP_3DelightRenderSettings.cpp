#define __TBB_show_deprecation_message_atomic_H
#define __TBB_show_deprecation_message_task_H

#include "LOP_3DelightRenderSettings.h"

#include "lop_utilities.h"
#include "ui/aov.h"

#include <HUSD/HUSD_Utils.h>
#include <LOP/LOP_PRMShared.h>
#include <LOP/LOP_Error.h>
#include <OP/OP_OperatorTable.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OpTypeId.h>
#include <PRM/PRM_Include.h>
#include <PRM/PRM_Parm.h>
#include <UT/UT_HDKVersion.h>

const char* LOP_3DelightRenderSettings::k_camera = "camera";
const char* LOP_3DelightRenderSettings::k_resolution = "resolution";
static constexpr unsigned kNumCustomAOVSlots = 8;

/*
	Callback to make the output toggles behave like a radio button.
*/
static int OutputRadioCallback(
	void *data, int index, fpreal t, const PRM_Template *tplate)
{
	OP_Parameters *node = reinterpret_cast<OP_Parameters*>(data);
	const UT_StringRef &toggled = tplate->getNamePtr()->getTokenRef();
	if( node->evalInt(toggled, 0, t) )
	{
		const char *all_options[] = {
			"display_rendered_images",
			"save_rendered_images",
			"export_nsi_file",
		};
		for( const char *option : all_options )
		{
			if( toggled != option )
			{
				/* Unset all other options. */
				node->setInt(option, 0, t, false);
			}
		}
	}
	else
	{
		/* Unselecting is not allowed. */
		node->setInt(toggled, 0, t, true);
	}
	return 1;
}

static int RenderCB(
	void *i_node, int i_index, double i_time, const PRM_Template *i_template)
{
	reinterpret_cast<LOP_3DelightRenderSettings*>(i_node)->DoRender(i_time);
	return 0; /* no need to refresh dialog */
}

PRM_Template* LOP_3DelightRenderSettings::GetTemplates()
{
	static std::vector<PRM_Template> templates;
	if( !templates.empty() )
	{
		/* This should only be called once. But it is static content anyway so
		   we return it again just in case. */
		assert(false);
		return templates.data();
	}

	{
		static PRM_Name name("execute_render", "          Render          ");
		templates.emplace_back(
			PRM_CALLBACK, 1, &name, nullptr, nullptr, nullptr, &RenderCB);
	}

	{
		static PRM_Default def(0, "/Render/rendersettings");
		templates.emplace_back(
			PRM_STRING, 1, &lopPrimPathName, &def, &lopPrimPathMenu,
			nullptr, PRM_Callback{}, &lopPrimPathSpareData);
	}
	{
		static PRM_Name name("camera", "Camera Path");
		static PRM_Default def(0, "/cameras/camera1");
		templates.emplace_back(
			PRM_STRING, 1, &name, &def, nullptr, nullptr, PRM_Callback{},
			&lopPrimPathSpareData);
	}
	{
		static PRM_Name name("resolution", "Resolution");
		templates.emplace_back(PRM_INT, 2, &name);
	}

    std::vector<PRM_Template> shadingComponentLayers;
    std::vector<PRM_Template> auxiliaryVariableLayers;
	{
		const auto& descriptions = aov::getDescriptions();
		static std::vector<PRM_Name> names;
		/* Necessary so objects don't move around as we add more. */
		names.reserve(descriptions.size());
		for (const auto& desc : descriptions)
		{
			std::string parm_name = desc.m_variable_name;
			std::replace(parm_name.begin(), parm_name.end(), '.', '_');
			names.emplace_back(
				PRM_Name::COPY, parm_name.c_str(), desc.m_ui_name.c_str());
			PRM_Name* aovLayerName = &names.back();

			static PRM_Default s_true_default{1};
			PRM_Default *default_val = nullptr;
			/* Ci (the beauty pass) is enabled by default. */
			if( desc.m_variable_name == "Ci" )
				default_val = &s_true_default;

			if(desc.m_type == aov::e_shading)
			{
				shadingComponentLayers.emplace_back(
					PRM_TOGGLE, 1, aovLayerName, default_val);
			}
			else if (desc.m_type == aov::e_auxiliary)
			{
				auxiliaryVariableLayers.emplace_back(
					PRM_TOGGLE, 1, aovLayerName, default_val);
			}
		}
	}

	std::vector<PRM_Template> customVariableLayers;
	{
		const char *help = "Enter the name of an AOV defined by the AOV Group "
			"shader node.";
		static std::vector<PRM_Name> names;
		/* Necessary so objects don't move around as we add more. */
		names.reserve(kNumCustomAOVSlots);
		for( unsigned i = 1; i <= kNumCustomAOVSlots; ++i )
		{
			std::string number = std::to_string(i);
			std::string parm_name = "custom_aov_" + number;
			std::string parm_label = "Output Variable " + number;
			names.emplace_back(PRM_Name::COPY, parm_name, parm_label);
			customVariableLayers.emplace_back(
				PRM_STRING, 1, &names.back(), nullptr, nullptr, nullptr,
				PRM_Callback{}, nullptr, 1, help);
		}
	}

	/* Top level tabs definition. */
	{
		static PRM_Name name("top_level_tabs");
		static std::array<PRM_Default, 3> tabs =
		{
			PRM_Default(8, "Output"),
			PRM_Default(1, "Image Layers"),
			PRM_Default(1, "Overrides"),
		};
		templates.emplace_back(
			PRM_SWITCHER, int(tabs.size()), &name, tabs.data());
	}

	/* Output tab begins here. */
	{
		static PRM_Name name("display_rendered_images", "Display");
		static PRM_Default def(true);
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, &OutputRadioCallback);
	}
	{
		static PRM_Name name("save_rendered_images", "Image File");
		static PRM_Default def(false);
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, &OutputRadioCallback);
	}
	{
		static PRM_Name name("export_nsi_file", "NSI File");
		static PRM_Default def(false);
		templates.emplace_back(
			PRM_TOGGLE, 1, &name, &def, nullptr, nullptr, &OutputRadioCallback);
	}
	{
		static PRM_Name name("output_separator1", "");
		templates.emplace_back(PRM_SEPARATOR, 0, &name);
	}
	{
		static PRM_Name name("image_filename", "Image Filename");
		static PRM_Default def(0.0f, "$HIP/render/`$HIPNAME`_`$OS`_$F4.exr");
		templates.emplace_back(
			PRM_FILE, 1, &name, &def, nullptr, nullptr, nullptr,
			&PRM_SpareData::fileChooserModeWrite);
	}
	{
		static PRM_Name name("image_format", "Image Format");
		static PRM_Default def(0.0f, "exr");
		static PRM_Item formats[] =
		{
			PRM_Item("exr", "OpenEXR"),
			PRM_Item("deepexr", "OpenEXR (deep)"),
			PRM_Item("deepalphaexr", "OpenEXR (deep alpha only)"),
			PRM_Item("dwaaexr", "OpenEXR (DWAA)"),
			PRM_Item("deepalphadwaaexr", "OpenEXR (DWAA+deep alpha)"),
			PRM_Item()
		};
		static PRM_ChoiceList format_choices(PRM_CHOICELIST_SINGLE, formats);
		templates.emplace_back(
			PRM_STRING, 1, &name, &def, &format_choices);
	}
	{
		static PRM_Name name("output_separator2", "");
		templates.emplace_back(PRM_SEPARATOR, 0, &name);
	}
	{
		static PRM_Name name("nsi_filename", "NSI Filename");
		static PRM_Default def(0.0f, "$HIP/render/`$HIPNAME`_`$OS`_$F4.nsi");
		templates.emplace_back(
			PRM_FILE, 1, &name, &def, nullptr, nullptr, nullptr,
			&PRM_SpareData::fileChooserModeWrite);
	}

	/* Image Layers tab begins here. */
	{
		static PRM_Name name("image_subtabs");
		static std::vector<PRM_Default> tabs =
		{
			PRM_Default(shadingComponentLayers.size(), "Shading Components"),
			PRM_Default(auxiliaryVariableLayers.size(), "Auxiliary Variables"),
			PRM_Default(customVariableLayers.size(), "Custom Variables"),
		};

		templates.emplace_back(
			PRM_SWITCHER, int(tabs.size()), &name, tabs.data());

		templates.insert(templates.end(),
			shadingComponentLayers.begin(), shadingComponentLayers.end());

		templates.insert(templates.end(),
			auxiliaryVariableLayers.begin(), auxiliaryVariableLayers.end());

		templates.insert(templates.end(),
			customVariableLayers.begin(), customVariableLayers.end());
	}

	/* Overrides tab begins here. */
	{
		static PRM_Name name("enable_depth_of_field", "Enable Depth of Field");
		static PRM_Default def(true);
		templates.emplace_back(PRM_TOGGLE, 1, &name, &def);
	}

    templates.push_back(PRM_Template());

    return templates.data();
}

void
LOP_3DelightRenderSettings::Register(OP_OperatorTable* io_table)
{
    OP_Operator *op = new OP_Operator(
        "3delightrendersettings",
        "3Delight Render Settings",
        LOP_3DelightRenderSettings::alloc,
        LOP_3DelightRenderSettings::GetTemplates(),
        LOP_SubNet::theChildTableName,
        0, 1, nullptr,
        OP_FLAG_GENERATOR | OP_FLAG_NETWORK);

    op->setIconName("ROP_3Delight");
    io_table->addOperator(op);
}

OP_Node *
LOP_3DelightRenderSettings::alloc(
	OP_Network *net, const char *name, OP_Operator *op)
{
    return new LOP_3DelightRenderSettings(net, name, op);
}

const char *
LOP_3DelightRenderSettings::getChildType() const
{
    return OPtypeIdToString(LOP_OPTYPE_ID);
}

OP_OpTypeId
LOP_3DelightRenderSettings::getChildTypeID() const
{
    return LOP_OPTYPE_ID;
}

LOP_3DelightRenderSettings::LOP_3DelightRenderSettings(
	OP_Network *net, const char *name, OP_Operator *op)
:
	LOP_SubNet(net, name, op)
{
}

LOP_3DelightRenderSettings::~LOP_3DelightRenderSettings()
{
}

bool
LOP_3DelightRenderSettings::runCreateScript()
{
	bool ret = LOP_SubNet::runCreateScript();

	//Create necessary nodes for our network.
	OP_Node *lop_render_var = createNode("3delightstandardrendervars");
	OP_Node *custom_vars = createCustomVarsNetwork();
	OP_Node *render_product = createNode("renderproduct", "renderproduct");
	OP_Node *stream_product = createNode("renderproduct", "stream_renderproduct");
	OP_Node *render_settings = createNode("rendersettings", "rendersettings");
	OP_Node *output = createNode("output", "Output");
	OP_Node *usdrender_rop = createNode("usdrender_rop", "usdrender_rop");

	lop_render_var->runCreateScript();
	render_product->runCreateScript();
	stream_product->runCreateScript();
	render_settings->runCreateScript();
	output->runCreateScript();
	usdrender_rop->runCreateScript();

    //Set node's parameters. Fix this!
    setInt("resolution",0,0,1280);
    setInt("resolution",1,0,720);

    //set rendersettings parameters from 3Delight LOPRenderSettings node.
	render_settings->setString(
		"chs(\"../primpath\")", CH_OLD_EXPRESSION, "primpath", 0, 0);
    render_settings->setString("chs(\"../camera\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"camera",0,0);
    render_settings->setString("chs(\"../resolution1\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"resolution",0,0);
    render_settings->setString("chs(\"../resolution2\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"resolution",1,0);
    render_settings->setString("chs(\"../shading_samples\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalshadingsamples_gebg",0,0);
    render_settings->setString("chs(\"../pixel_samples\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalpixelsamples_69ag",0,0);
    render_settings->setString("chs(\"../volume_samples\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalvolumesamples_tcbg",0,0);
    render_settings->setString("chs(\"../max_diffuse_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumdiffusedepth_lmbg",0,0);
    render_settings->setString("chs(\"../max_reflection_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumreflectiondepth_hrbg",0,0);
    render_settings->setString("chs(\"../max_refraction_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumrefractiondepth_hrbg",0,0);
    render_settings->setString("chs(\"../max_hair_depth\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumhairdepth_phbg",0,0);
    render_settings->setString("chs(\"../max_distance\")",CH_EXPRESSION_IN_DEFAULT_LANGUAGE,"xn__nsiglobalmaximumdistance_2fbg",0,0);

    //Set NSI parameters on usd rendersettings child node. Allows these parameters to be exported by usd.
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalshadingsamples_control_hrbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalpixelsamples_control_7nbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalvolumesamples_control_upbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumdiffusedepth_control_mzbg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumreflectiondepth_control_i4bg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumrefractiondepth_control_i4bg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumhairdepth_control_qubg",0,0);
    render_settings->setString("set",CH_STRING_LITERAL,"xn__nsiglobalmaximumdistance_control_3sbg",0,0);

	/* The main image product is always rendered but the stream product depends
	   on "NSI File" being selected. */
	render_settings->setString(
		"/Render/Products/renderproduct"
		" `ifs(ch(\"../export_nsi_file\"), "
			"\"/Render/Products/stream_renderproduct\", \"\")`",
		CH_STRING_LITERAL, "products", 0, 0);

	/* Connect overrides to rendersettings.*/
	render_settings->setString("set", CH_STRING_LITERAL,
		"xn__nsiglobalenabledepthoffield_control_zxbg", 0, 0);
	render_settings->setString(
		"ch(\"../enable_depth_of_field\")", CH_OLD_EXPRESSION,
		"xn__nsiglobalenabledepthoffield_ykbg", 0, 0);

	/* Build an expression for render products. */
    const auto& descriptions = aov::getDescriptions();
    std::string result = "";
    for (const auto& desc : descriptions)
    {
        std::string label_name = desc.m_variable_name;
        std::replace(label_name.begin(), label_name.end(), '.', '_');
        result += "`ifs(ch(\"../" + label_name + "\"), \"/Render/Products/Vars/" + label_name + "\", \"\")` ";
    }
	for( unsigned i = 1; i <= kNumCustomAOVSlots; ++i )
	{
		std::string node_name = "custom_aov" + std::to_string(i);
		std::string parm_name = "../custom_aov_" + std::to_string(i);
		result += "`ifs(0 != strlen(chs(\"" + parm_name +
			"\")), \"/Render/Products/Vars/" + node_name + "\", \"\")` ";
	}

    //Set all AOVs in orderedVars parameter of renderproduct node. Maybe a better solution can be implemented? 
    render_product->setString(result.c_str(),CH_STRING_LITERAL,"orderedVars",0,0);

	/* Set image file name. */
	render_product->setString(
		"chs(\"../image_filename\")", CH_OLD_EXPRESSION, "productName", 0, 0);

	/* Change product type based on "Display" toggle. */
	render_product->setString(
		"ifs(ch(\"../display_rendered_images\"), \"nsi:display\", "
			"strcat(\"nsi:\", chs(\"../image_format\")))",
		CH_OLD_EXPRESSION, "productType", 0, 0);

	/* Setup the nsi stream renderproduct. */
	stream_product->setString(
		"chs(\"../nsi_filename\")", CH_OLD_EXPRESSION, "productName", 0, 0);
	stream_product->setString(
		"nsi:apistream", CH_STRING_LITERAL, "productType", 0, 0);

	/* Configure the usdrender node for 3Delight. */
	usdrender_rop->setString(
		"HdNSIRendererPlugin", CH_STRING_LITERAL, "renderer", 0, 0);
	usdrender_rop->setString(
		"chs(\"../rendersettings/primpath\")",
		CH_OLD_EXPRESSION, "rendersettings", 0, 0);

	/* Connect out internal network. */
	lop_utilities::ConnectInput(lop_render_var, nullptr);
	custom_vars->setInput(0, lop_render_var);
	render_product->setInput(0, custom_vars);
	stream_product->setInput(0, render_product);
	render_settings->setInput(0, stream_product);
	output->setInput(0, render_settings);
	usdrender_rop->setInput(0, render_settings);

	lop_utilities::LayoutNodes(this);
    finishedLoadingNetwork();
	return ret;
}

/*
	Trigger the Render button of our usdrender_rop node.
*/
void LOP_3DelightRenderSettings::DoRender(double i_time)
{
	OP_Node *usdrender = getChild("usdrender_rop");
	if( !usdrender )
		return;

	PRM_Parm *parm = usdrender->getParmPtr("execute");
	if( !parm )
		return;

	usdrender->triggerParmCallback(parm, i_time, 0, usdrender);
}

void
LOP_3DelightRenderSettings::PRIMPATH(UT_String &str, fpreal t)
{
    evalString(str, lopPrimPathName.getToken(), 0, t);
    HUSDmakeValidUsdPath(str, true);
}

void
LOP_3DelightRenderSettings::getCamera(UT_String &str, fpreal t)
{
    evalString(str, k_camera, 0, t);
    HUSDmakeValidUsdPath(str, true);
}

OP_ERROR
LOP_3DelightRenderSettings::cookMyLop(OP_Context &context)
{
#if HDK_API_VERSION >= 19050000
    syncDelayedOTL();
    UT_ASSERT(!getDelaySyncOTL());
#endif
    return cookReferenceNode(context, getNodeForSubnetOutput(0));
}

OP_Node* LOP_3DelightRenderSettings::createCustomVarsNetwork()
{
	LOP_Node *net = CAST_LOPNODE(createNode("subnet", "customvars"));

	OP_Node *prev_node = nullptr;
	for( unsigned i = 1; i <= kNumCustomAOVSlots; ++i )
	{
		std::string node_name = "custom_aov" + std::to_string(i);
		std::string parm_name = "../../custom_aov_" + std::to_string(i);
		OP_Node *aov_node = net->createNode("rendervar", node_name.c_str());

		std::string source_expr =
			"strcat(\"shader:\", chs(\"" + parm_name + "\"))";
		aov_node->setString(
			source_expr.c_str(), CH_OLD_EXPRESSION, "sourceName", 0, 0);
		std::string name_expr = "chs(\"" + parm_name + "\")";
		aov_node->setString(
			name_expr.c_str(), CH_OLD_EXPRESSION,
			"xn__driverparametersaovname_jebkd", 0, 0);
		aov_node->setString(
			name_expr.c_str(), CH_OLD_EXPRESSION,
			"xn__driverparametersaovchannel_prefix_tubkd", 0, 0);
		aov_node->setString(
			"color3f", CH_STRING_LITERAL, "dataType", 0, 0);
		aov_node->setString(
			"color3h", CH_STRING_LITERAL,
			"xn__driverparametersaovformat_shbkd", 0, 0);

		lop_utilities::ConnectInput(aov_node, prev_node);
		prev_node = aov_node;
	}

	OP_Node *output = net->createNode("output", "Output");
	lop_utilities::ConnectInput(output, prev_node);

	lop_utilities::LayoutNodes(net);
	return net;
}
