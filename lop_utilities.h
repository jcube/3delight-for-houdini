#ifndef __lop_utilities
#define __lop_utilities

class OP_Network;
class OP_Node;

namespace lop_utilities
{
void LayoutNodes(OP_Network *net);
void ConnectInput(OP_Node *node, OP_Node *input_node);
}

#endif
